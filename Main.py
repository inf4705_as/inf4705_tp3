import argparse, os, sys, subprocess

os.environ["ANT_OPTS"] = "-Xmx2048m"
subprocess.call(["ant", "-q", "-S", "clean"]);
subprocess.call(["ant", "-q", "-S","jar"])

parser = argparse.ArgumentParser(description='This program will run the last work for the class INF4705')
parser.add_argument('-e', '--exemplaire', nargs='*', help='Chose your file to consider. These path must be absolute!')
parser.add_argument('-p', '--print', action="count", help='Print the best path so far')
args = parser.parse_args()

fichierPCT = [
    "PCT_20_50", 
    "PCT_50_10", "PCT_50_25", "PCT_50_50", 
    "PCT_100_10", "PCT_100_25", "PCT_100_50", 
    "PCT_200_10", "PCT_200_25", "PCT_200_50"
    ]

isE = args.exemplaire is not None
isP = args.print is not None
e_path = ""


def iterFunc(algo):
    
    for fichier in fichierPCT:
        txt_fichier = str(fichier)
        # print("txt_fichier : " + str(txt_fichier))
        subprocess.call(
            [
                "java", "-Xmx1g", "-Xms1g", "-jar", "build/jar/"+ algo +".jar", 
                str(isP), str(isE), str(txt_fichier)
            ]
        )
    pass

if isE :
    if len(args.exemplaire) == 1:
        e_path = args.exemplaire[0]
    else:
        print("Option -e : Pas assez ou trop de path")

if(not isE):
    iterFunc("AlgoRapide")
else:
    subprocess.call(
            [
                "java", "-Xmx1g", "-Xms1g", "-jar", "build/jar/AlgoRapide.jar", 
                str(isP), str(isE), str(e_path)
            ]
        )

subprocess.call(["ant", "-q", "-S", "clean"]);
