Lancer le code avec la commande suivante : 

*./tp.sh -e ./src/ressources/instances/PCT_200_50 > src/ressources/solutions/sol_200_50.txt -p*

Le code va rouler tant est aussi longtemps que l'utilisateur n'a pas intérompu le programme (habituellement avec *Ctrl+C* )

Si l'option __-p__ n'est pas ajouter, les scores seront affichés seulement et non les chemins.

Pour vérifier si les résultats avec l'option __-p__ sont correct, lancer la commande suivante : 

*python3 sol_check.py ./src/ressources/instances/PCT_200_50 src/ressources/solutions/sol_200_50.txt*
