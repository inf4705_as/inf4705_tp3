package ca.inf4705.tp3.utils;

import java.io.File;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;

import java.util.*;
import java.util.Collections;

import ca.inf4705.tp3.utils.*;

/**
 * Created by abmala on 18-09-14.
 */
public class ReadingPCT {
    public ReadingPCT() {
    }

    public void println(int[][] array) {
        for (int i = 0; i < array.length; ++i) {
            for(int j=0; j< array[i].length ; ++j){
                System.out.print(array[i][j] + "  ");
            }
            System.out.println();
        }
    }

    public void println(Integer[][] array) {
        for (int i = 0; i < array.length; ++i) {
            for(int j=0 ; j < array[i].length; ++j){
                System.out.print(array[i][j] + "  ");
            }
            System.out.println();
        }
    }

    public void println(ArrayList<ArrayList<Integer>> array) {
        for (int i = 0; i < array.size(); ++i) {
            ArrayList<Integer> intArray = array.get(i);
            System.out.println("" + intArray.toString());
            System.out.println();
        }
    }

    static String folder = System.getProperty("user.dir") + "/src/ressources/instances/";

    public Exemplaire read_func(String fileName_input, boolean isAbsolue) throws IOException {
        File file;
        int n_nombre_attraction=0;
        int[][] matrice_adjacente = new int[0][0];
        if(isAbsolue){
            file = new File(fileName_input);
        }else{
            file = new File(ReadingPCT.folder + fileName_input);
        }
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        if ((line = bufferedReader.readLine()) != null) {
            n_nombre_attraction = Integer.parseInt(line);
            matrice_adjacente = new int[n_nombre_attraction][n_nombre_attraction];// [2];
            int index = 0;
            while (index < n_nombre_attraction && (line = bufferedReader.readLine()) != null) {
                String[] sT = line.split(" ");
                for(int i=0; i< sT.length ; ++i){
                    matrice_adjacente[index][i] = Integer.parseInt(sT[i]);
                }
                ++index;
            }
        }
        int t_max = 0;
        if ((line = bufferedReader.readLine()) != null) {
            t_max = Integer.parseInt(line);
        }
        
        int[] appreciation = new int[n_nombre_attraction];
        if ((line = bufferedReader.readLine()) != null) {
            String[] sT = line.split(" ");
            for(int i=0; i< sT.length ; ++i){
                appreciation[i] = Integer.parseInt(sT[i]);
            }
        }

        Exemplaire exemplaire = new Exemplaire(n_nombre_attraction, matrice_adjacente, t_max, appreciation, file.getAbsolutePath());
        return exemplaire;
    }

    public static void main(String[] args) {
        ReadingPCT pr = new ReadingPCT();
        Exemplaire exemplaire;
        try{
            exemplaire = pr.read_func("PCT_20_50", false);
            System.out.println(exemplaire.toString());
        }
        catch(IOException io){
            System.err.println("Error reading it");
            exemplaire = null;
        }

        if(exemplaire != null){

            HashMap<Integer, Integer> hm = new HashMap<>();

            for(int i=0 ; i < exemplaire.get_appreciation().length ; ++i){
                hm.put(i, exemplaire.get_appreciation()[i]);
            }

            System.out.println("Avant hm : \n" + hm.toString());

            // hm.remove(10);

            // System.out.println("Apres hm : \n" + hm.toString());

            // Key key = Collections.max(hm.entrySet(), Map.Entry.comparingByValue()).getKey();

            Integer key = Collections.max(hm.entrySet(), Map.Entry.comparingByValue()).getKey();

            System.out.println("key : " + key);

            Attraction[] ensemble_attractions = new Attraction[exemplaire.get_n_nombre_attraction()];

            for(int i=0; i < exemplaire.get_n_nombre_attraction(); ++i ){
                int[] distance_attraction = exemplaire.get_matrice_adjacente()[i];
                int appreciation = exemplaire.get_appreciation()[i];
                ensemble_attractions[i] = new Attraction(distance_attraction, appreciation);
            }

            for(int i=0; i<exemplaire.get_n_nombre_attraction(); ++i){
                String donnee_attraction = ensemble_attractions[i].toString();
                // System.out.println("" + donnee_attraction);
            }
        }
    }
}
