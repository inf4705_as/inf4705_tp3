package ca.inf4705.tp3.utils;

import java.util.*;

public class Exemplaire {
    private String filePath;
    private int n_nombre_attraction;
    private int[][] matrice_adjacente;
    private int t_max;
    private int[] appreciation;

    public Exemplaire(int n_nombre_attraction, int[][] matrice_adjacente, int t_max, int[] appreciation,
            String filePath) {
        this.n_nombre_attraction = n_nombre_attraction;
        this.matrice_adjacente = matrice_adjacente;
        this.t_max = t_max;
        this.appreciation = appreciation;
        this.filePath = filePath;
    }

    public int get_n_nombre_attraction() {
        return this.n_nombre_attraction;
    }

    public int[][] get_matrice_adjacente() {
        return this.matrice_adjacente;
    }

    public int get_t_max() {
        return this.t_max;
    }

    public void setTime(int time) {
        this.t_max = time;
    }

    public int[] get_appreciation() {
        return this.appreciation;
    }

    public String toString() {

        String str = "\nExemplaire\n";
        str += "\n--> file_path : " + this.filePath;
        str += "\n--> n_nombre_attraction : " + this.n_nombre_attraction;
        str += "\n--> t_max : " + this.t_max;
        str += "\n--> appreciation : \t";
        for (int i = 0; i < this.appreciation.length; ++i) {
            str += this.appreciation[i] + "  ";
        }
        str += "\n--> matrice_adjacente : \n";
        for (int i = 0; i < this.matrice_adjacente.length; ++i) {
            str += "\t";
            for (int j = 0; j < this.matrice_adjacente[i].length; ++j) {
                str += this.matrice_adjacente[i][j] + "  ";
            }
            str += "\n";
        }

        return str;
    }
}