package ca.inf4705.tp3.utils;
import java.util.*;

public class SolutionRetourner{
    private ArrayList<Integer> solution;
    private int temps;

    public SolutionRetourner(ArrayList<Integer> solution, int temps){
        this.solution = new ArrayList<>(solution);
        this.temps = temps;
    }

    public int get_temps(){
        return this.temps;
    }

    public ArrayList<Integer> get_solution(){
        return this.solution;
    }
}