package ca.inf4705.tp3.utils;

import ca.inf4705.tp3.utils.*;
import ca.inf4705.tp3.algo.Glouton;
import java.util.ArrayList;

public class Voisin {

    private ArrayList<Integer> list_center_not_used;
    private ArrayList<Integer> list_center_used;
    private int time_left;

    public Voisin(){}
    
    public Voisin(
        int time_left, 
        ArrayList<Integer> list_center_used, 
        ArrayList<Integer> list_center_not_used
        )
    {
        this.list_center_not_used = list_center_not_used;
        this.list_center_used = list_center_used;
        this.time_left = time_left;
    }

    public ArrayList<Integer> get_list_center_not_used(){
        return this.list_center_not_used;
    }

    public ArrayList<Integer> get_list_center_used(){
        return this.list_center_used;
    }

    public int get_time_left(){
        return this.time_left;
    }

    public void set_list_center_not_used(ArrayList<Integer> list_center_not){
        this.list_center_not_used = list_center_not;
    }

    public void set_list_center_used(ArrayList<Integer> list_center_used){
        this.list_center_used = list_center_used;
    }

    public void set_time_left(int time){
        this.time_left = time;
    }

}