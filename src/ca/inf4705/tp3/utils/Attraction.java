package ca.inf4705.tp3.utils;

import java.util.*;

public class Attraction{
    // C'est une valeur static qui va initialiser l'id a chaque creation d'objet Attraction.
    private static int index=0;
    // La valeur id correspond a l'index qu'aura l'objet Attraction dans le tableau ou 
    // il sera store.
    private int id;
    // distance_attraction : sert a contenir les distances de toutes les autres attractions 
    // contenu dans la ville par rapport a l'attraction actuel que nous creeons.
    private int[] distance_attraction;
    // appreciation : Correspond a la valeur donnee a une attraction en fonction de 
    // son attirance/qualitee evaluees par les touristes
    private int appreciation;

    public Attraction(int[] distance_attraction, int appreciation){
        // On initialise le id de l'objet actuel en appelant l'attribut static index.
        //
        // Cet attribut va mettre sa valeur actuel d'index de tableau dans id et 
        // sera incrementer pour les prochains objets.
        //
        // Etant donnee que cet attribut est static, il est partage par tout les 
        // objets de type Attraction.
        //
        this.id = Attraction.index++;
        this.distance_attraction = distance_attraction;
        this.appreciation = appreciation;
    }

    public int get_id(){
        return this.id;
    }

    public int[] get_distance_attraction(){
        return this.distance_attraction;
    }

    public int get_appreciation(){
        return this.appreciation;
    }

    public String toString(){
        String str = "";
        str += "\nAttraction " + this.id  + " : \n";
        str += "\n--> valeur de l'attraction : " + this.appreciation;
        str += "\n--> distances de attraction " + this.id + " envers les autres : ";
        str += "\n\t";
        for(int i=0 ; i < this.distance_attraction.length ; ++i){
            str += "  " + this.distance_attraction[i];
        }
        str += "\n";
        return str;
    }
}