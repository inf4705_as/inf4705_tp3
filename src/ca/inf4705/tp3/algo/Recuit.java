package ca.inf4705.tp3.algo;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import ca.inf4705.tp3.utils.*;

// But maximiser la quantite d'appreciation
public class Recuit {

    private Attraction[] attractions;

    public Recuit(Attraction[] attractions) {
        this.attractions = attractions;
    }

    public SolutionRetourner recuit(ArrayList<Integer> solutionInitial, int time_left, Boolean isP) {

        float heat_up = 800.0f;
        float teta_down_limit = 0.5f;
        int kmax    = 10000;
        int p       = 15000;
        float alpha = 0.90f;
        float teta  = 800.0f;
        int Dif;
        ArrayList<Integer> id_list_used = new ArrayList<Integer>(solutionInitial);
        ArrayList<Integer> bestSolution = new ArrayList<Integer>(id_list_used);
        ArrayList<Integer> id_list_not_used = new ArrayList<Integer>(this.attractions.length);
        int time_temp = time_left;
        int bestSolutionTime = time_temp;
        for (int i = 0; i < this.attractions.length; ++i) {
            id_list_not_used.add(i);
        }
        Voisin newVoisin = new Voisin();
        Voisin tempVoisin = new Voisin();

        id_list_not_used.removeAll(id_list_used);

        tempVoisin.set_list_center_not_used(id_list_not_used);
        tempVoisin.set_time_left(time_left);
        tempVoisin.set_list_center_used(id_list_used);

        String result_ = tempVoisin.get_list_center_used().toString();
        result_ = result_.replace("[", "");
        result_ = result_.replace("]", "");
        result_ = result_.replaceAll(",", "");

        if(isP){
            System.out.println(result_);
        }else{
            System.out.println(somme(tempVoisin.get_list_center_used()));
        }

        for (int i = 0; i < kmax; i++) {
            for (int j = 0; j < p; j++) {
                int sum_solution = somme(id_list_used);
               newVoisin = calculate_voisin(tempVoisin);
                int sum_newvoisin = somme(newVoisin.get_list_center_used());

                Dif = sum_newvoisin - sum_solution;
                if (Dif >= 0 || calcul(Dif, teta)) {
                    id_list_used = newVoisin.get_list_center_used();
                    id_list_not_used = newVoisin.get_list_center_not_used();
                    time_temp = newVoisin.get_time_left();
                    int sum_best = somme(bestSolution);
                    int sum_new_solution = somme(id_list_used);
                    if (sum_new_solution > sum_best) {
                        bestSolution = id_list_used;
                        bestSolutionTime = time_temp;
                        String best_result = id_list_used.toString();
                        best_result = best_result.replace("[", "");
                        best_result = best_result.replace("]", "");
                        best_result = best_result.replaceAll(",", "");
                        if(!isP){
                            System.out.println(sum_new_solution);
                        }
                        else{
                            System.out.println(best_result);// + "\t\t" + sum_new_solution);
                        }
                    }
                }
                tempVoisin.set_time_left(time_temp);
                tempVoisin.set_list_center_used(new ArrayList<Integer>(id_list_used));
                tempVoisin.set_list_center_not_used(new ArrayList<Integer>(id_list_not_used));
            }
            teta = teta * alpha;
            // Si teta qui est la temperature descend sous teta_down_limit, 
            // ca veut dire qu'il faut rechauffer la solution pour ameliorer 
            // la probabilite d'obtenir une solution optimal dans les dernieres 
            // solutions verifiers.
            if(teta < teta_down_limit){
                teta = heat_up;
            }
        }

        // String best_result = bestSolution.toString();
        // best_result = best_result.replace("[", "");
        // best_result = best_result.replace("]", "");
        // best_result = best_result.replaceAll(",", "");

        SolutionRetourner solutionFinal = new SolutionRetourner(bestSolution, bestSolutionTime);
        return (solutionFinal);
    }

    public int somme(ArrayList<Integer> array) {
        int i;
        int sum = 0;
        for (i = 0; i < array.size(); i++)
            sum += this.attractions[array.get(i)].get_appreciation();
        return sum;
    }

    public Voisin calculate_voisin(Voisin voisin) {

        int time_left = voisin.get_time_left();
        ArrayList<Integer> id_list_used = voisin.get_list_center_used();
        ArrayList<Integer> id_list_not_used = voisin.get_list_center_not_used();
        // On va cherche un index random dans la liste des id de centre non utiliser
        int index = new Random().nextInt(id_list_not_used.size());

        // On recupere ce id
        Integer new_id_restant = id_list_not_used.get(index);
        // On calule le nouveau temps

        int new_time = time_left
                // Cette ligne ajoute le temps pris pour retourner a l'hotel a partir l'ancien
                // derniere element
                + this.attractions[id_list_used.get(id_list_used.size() - 2)].get_distance_attraction()[0]
                // Cette ligne ajoute le temps que le nouvelle element prendra pour retourner a
                // l'hotel
                - this.attractions[new_id_restant].get_distance_attraction()[0]
                // Cette ligne ajoute le temps pris pour l'ancien derniere element pour aller au
                // nouveau element
                - this.attractions[new_id_restant].get_distance_attraction()[id_list_used.get(id_list_used.size() - 2)];
        id_list_used.remove(id_list_used.size() - 1);
        // On va ajouter ce nouveau id dans la liste des resultats
        id_list_used.add(new_id_restant);
        id_list_used.add(0);

        // On retire l'element de de la liste non utiliser car, il va etre utiliser...
        id_list_not_used.remove(new_id_restant);

        if (new_time < 0) {
            int index_id_Temp = 0;
            do {
                // On cherche un nouvel index aleatoire

                index_id_Temp = new Random().nextInt(id_list_used.size());
                if(index_id_Temp == 0)
                {
                    ++index_id_Temp;
                }else if(index_id_Temp == (id_list_used.size() - 1)){
                    --index_id_Temp;
                }
                // On sauvegarde l'id de l'element avant le centre qu'on est sur le point de
                // supprimer
                int element_next = index_id_Temp + 1;
                // On sauvegarde l'id de l'element apres le centre qu'on est sur le point de
                // supprimer
                int element_before = index_id_Temp - 1;
                // On ajoute l'element de la liste des id non utiliser
                id_list_not_used.add(id_list_used.get(index_id_Temp));
                // On supprime l'element de la liste des id utiliser
                Integer elementToRemove = id_list_used.get(index_id_Temp);
                int obj_element_before = id_list_used.get(element_before) ;
                int obj_element_next   = id_list_used.get(element_next) ;
                id_list_used.remove(elementToRemove);

                new_time = new_time + this.attractions[obj_element_before].get_distance_attraction()[elementToRemove]
                        + this.attractions[elementToRemove].get_distance_attraction()[obj_element_next]
                        - this.attractions[obj_element_before].get_distance_attraction()[obj_element_next];

            } while (new_time < 0);
        }

        voisin.set_time_left(new_time);
        voisin.set_list_center_not_used(id_list_not_used);
        voisin.set_list_center_used(id_list_used);
        return voisin;
    }

    public boolean calcul(int dif, float teta) {
        float temp = 0;

        temp = dif / teta;
        return Math.exp(temp) >= unif(0, 1);

    }

    public float unif(int a, int b) {
        Random r = new Random();
        float valRand = (r.nextFloat() * (b - a)) + a;
        return valRand;
    }
}