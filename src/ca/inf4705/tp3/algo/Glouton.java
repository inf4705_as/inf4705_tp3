package ca.inf4705.tp3.algo;

import java.util.Collections;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import ca.inf4705.tp3.utils.Exemplaire;
import javafx.print.Collation;
import ca.inf4705.tp3.utils.Attraction;

public class Glouton {
    private static int INDEX_HOTEL = 0;
    private Attraction[] attractions;
    private HashMap<Integer, Integer> appreciation_of_visited_centers;
    private ArrayList<Integer> index_visited_centers;
    // private ArrayList<Integer> index_not_visited;
    private ArrayList<Integer> solution;
    private int time_left;
    private int index_actual_attraction = 0;

    public Glouton(Attraction[] attractions, Exemplaire exemplaire) {
        this.attractions = attractions;
        this.appreciation_of_visited_centers = new HashMap<>();
        // On remplit la hashmap avec les bonnes key et les bonnes valeurs
        for (int i = 0; i < exemplaire.get_appreciation().length; ++i) {
            appreciation_of_visited_centers.put(i, exemplaire.get_appreciation()[i]);
        }
        this.index_visited_centers = new ArrayList<>();
        this.index_actual_attraction = 0;
        // this.index_not_visited = new ArrayList<>();
        this.time_left = exemplaire.get_t_max();
    }

    // public ArrayList<Integer> get_index_not_visited(){
    // return this.index_not_visited;
    // }

    public int get_time_left() {
        return this.time_left;
    }

    private int get_index_max_value() {
        Integer key_max_val = Collections
                .max(this.appreciation_of_visited_centers.entrySet(), Map.Entry.comparingByValue()).getKey();
        return key_max_val;
    }

    public ArrayList<Integer> get_solution() {
        return this.solution;
    }

    private int get_min_value() {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < this.attractions.length; i++) {
            if (this.attractions[i].get_appreciation() < min) {
                min = this.attractions[i].get_appreciation();
            }
        }
        return min;
    }

    public boolean legal(int pos_time_next, int pos_time_next_and_hotel, int time_left) {
        return (pos_time_next_and_hotel + pos_time_next) <= time_left;
    }

    public boolean is_done() {
        return this.appreciation_of_visited_centers.isEmpty();
    }

    public ArrayList<Integer> get_best_path(int max_time) {

        ArrayList<Integer> solution = new ArrayList<Integer>();
        solution.add(this.index_actual_attraction);
        int time_left = max_time;

        // System.out.println("map : " +
        // this.appreciation_of_visited_centers.toString());
        while (time_left > 0 && !this.is_done()) {

            int next_index = this.get_index_max_value();
            // On va cherche le centre d'interet dans laquelle on est actuellement
            Attraction actual_attraction = this.attractions[this.index_actual_attraction];
            // On va chercher la prochaine centre d'interet dans laquelle on considere
            // voyager en raison de sa grande valeur
            Attraction next_attraction = this.attractions[next_index];

            // On va cherche le temps entre le centre d'interet dans laquelle on est en ce
            // moment et le prochain centre d'interet
            // dans lequelle on considere voyager.
            int time_actu_and_next = actual_attraction.get_distance_attraction()[next_index];
            // On va cherche le temps du prochain centre d'interet dans lequelle on
            // considere voyager et l'hotel.
            int time_next_and_hotel = next_attraction.get_distance_attraction()[INDEX_HOTEL];

            // On supprime la valeur de la prochaine destination de notre tableau.
            this.appreciation_of_visited_centers.remove(next_index);
            // this.index_not_visited.add(next_index);

            if (legal(time_actu_and_next, time_next_and_hotel, time_left)) {
                // System.out.print( time_left + " - " + time_next_and_hotel + " = " +
                // (time_left - time_next_and_hotel) + "\t\t\t");
                // System.out.println(time_left + " - " + time_actu_and_next + " = " +
                // (time_left - time_actu_and_next));
                time_left -= time_actu_and_next;
                solution.add(next_index);
                // this.index_not_visited.remove(next_index);
                this.index_actual_attraction = next_index;
            }
        }
        // System.out.println("Time left = " + time_left);
        // System.out.println("Solution : " + solution.toString());
        this.time_left = time_left;
        this.solution = solution;
        return solution;
    }
}
