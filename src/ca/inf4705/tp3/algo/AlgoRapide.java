package ca.inf4705.tp3.algo;

import java.util.*;
import java.io.File;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import ca.inf4705.tp3.utils.*;
import ca.inf4705.tp3.algo.*;
import ca.inf4705.tp3.algo.Glouton;

public class AlgoRapide {

    private ArrayList<Integer> meilleur_chemin;
    private int valeur_max;

    public AlgoRapide() {
        this.meilleur_chemin = new ArrayList<>();
        this.valeur_max = 0;
    }

    // public ArrayList<Integer> algoRapide(Exemplaire exemplaire) {
    //     Glouton glouton;
    //     Attraction[] ensemble_attractions = this.initialisation_ensemble_attractions(exemplaire);
    //     glouton = new Glouton(ensemble_attractions, exemplaire);
    //     return glouton.get_best_path(exemplaire.get_t_max());
    // }

    public SolutionRetourner algoLent(Exemplaire exemplaire, Boolean isP) {
        Attraction[] ensemble_attractions = this.initialisation_ensemble_attractions(exemplaire);
        Recuit recuit = new Recuit(ensemble_attractions);
        Glouton glouton = new Glouton(ensemble_attractions, exemplaire);
        return recuit.recuit(glouton.get_best_path(exemplaire.get_t_max()), glouton.get_time_left(), isP);
    }

    public SolutionRetourner algoLent(Exemplaire exemplaire, ArrayList<Integer> solution, int temps, Boolean isP) {
        Attraction[] ensemble_attractions = this.initialisation_ensemble_attractions(exemplaire);
        Recuit recuit = new Recuit(ensemble_attractions);
        return recuit.recuit(solution, temps, isP);
    }

    private Attraction[] initialisation_ensemble_attractions(Exemplaire exemplaire) {
        // Creation d'un tableau avec la taille du nombre d'attraction qu'il y a dans la
        // ville.
        Attraction[] ensemble_attractions = new Attraction[exemplaire.get_n_nombre_attraction()];

        // Remplissage du tableau ensemble_attractions
        for (int i = 0; i < exemplaire.get_n_nombre_attraction(); ++i) {
            // On va chercher les distances des autres attractions
            // envers l'attraction de l'index i.
            int[] distance_attraction = exemplaire.get_matrice_adjacente()[i];
            // On va chercher la valeur de l'attraction a l'index i.
            int appreciation = exemplaire.get_appreciation()[i];
            // On cree l'attraction de l'index i et la met a ce meme index.
            ensemble_attractions[i] = new Attraction(distance_attraction, appreciation);
        }
        return ensemble_attractions;
    }

    public static void main(String[] args) {

        double start_time = (double) System.nanoTime();
        boolean isP = false;
        boolean isE = false;
        String e_path = "";
        AlgoRapide algo_rapide = null;

        if (args.length == 3) {
            isP = Boolean.valueOf(args[0]);
            isE = Boolean.valueOf(args[1]);
            e_path = args[2];
        } else {
            isP = true;
            isE = false;
            e_path = "PCT_200_50";
        }

        // System.out.println("*******************************");
        // System.out.println("File : " + e_path.toString());
        ReadingPCT pr = new ReadingPCT();
        Exemplaire exemplaire;
        try {
            exemplaire = pr.read_func(e_path, isE);
            // System.out.println(exemplaire.toString());
        } catch (IOException io) {
            System.err.println("Error reading it");
            exemplaire = null;
        }

        if (exemplaire != null) {
            algo_rapide = new AlgoRapide();
            // algo_rapide.meilleur_chemin = algo_rapide.algoRapide(exemplaire);
            SolutionRetourner solution_final = algo_rapide.algoLent(exemplaire, isP);
            algo_rapide.meilleur_chemin = solution_final.get_solution();
            while(true)
            {
                solution_final = algo_rapide.algoLent(exemplaire, solution_final.get_solution(), solution_final.get_temps(), isP);
            }
        }

        // Si l'utilisateur a specifier l'option -e,
        // le chemin va s'imprimer
        // if (isE) {
        //     System.out.println("The path is : " + e_path);
        // }

        // Si l'utilisateur a specifier l'option -p,
        // le meilleur chemin trouver a chaque fois va s'afficher.
        // if (isP) {
        //     System.out.println("Best path : " + algo_rapide.meilleur_chemin.toString());
        // }
        // if(!isP){
        //     System.out.println("" + algo_rapide.meilleur_chemin.toString());
        // }
        double end_time = System.nanoTime();
        double total_time = (end_time - start_time) / 1e9;

        // System.out.println(total_time + " seconds");
    }
}